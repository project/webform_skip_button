(function ($) {
  Drupal.behaviors.webform_skip_button = {
    attach: function (context, settings) {
      $(".webform-skip-button").click(function (e) {
        var skip = $(this).attr('id');
        if (typeof skip == "string") {
          e.preventDefault();
          $('html, body').animate({
            scrollTop: $("input[id=" + skip + "").offset().top
          }, 2000);
        }
      });
    }
  };
}(jQuery));