<?php

/**
 * @file
 * Webform Skip module skip component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_skip_button() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'weight' => 0,
    'page_num' => 0,
    'extra' => array(
      'private' => FALSE,
      'skip_button_label' => '',
      'skip_button_page' => '',
      'skip_button_alignment' => '',
    ),
  );
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_skip_button() {
  return array(
    'webform_display_skip' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_skip_button($component) {
  $form = array();

  $form['extra']['skip_button_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Skip page button label'),
    '#description' => t('This is used to add a button to skip to a particular spot on the form. If left blank the button will be labeled Skip.'),
    '#default_value' => $component['extra']['skip_button_label'],
    '#size' => 30,
  );

  $options = _webform_skip_button_get_page_options($component);
  $form['extra']['skip_button_page'] = array(
    '#type' => 'select',
    '#title' => t('Field to skip to'),
    '#description' => t('Select field to skip to on the form.'),
    '#options' => $options,
    '#default_value' => $component['extra']['skip_button_page'],
  );

  $form['extra']['skip_button_alignment'] = array(
    '#type' => 'select',
    '#title' => t('Button alignment'),
    '#description' => t('Where to place button on the form'),
    '#options' => array(
      'left' => t('Left'),
      'right' => t('Right'),
    ),
    '#default_value' => $component['extra']['skip_button_alignment'],
  );

  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_skip_button($component, $value = NULL, $filter = TRUE, $submission = NULL) {
  $skip_button_alignment = !empty($component['extra']['skip_button_alignment']) ? $component['extra']['skip_button_alignment'] : '';
  $skip_button_trigger = !empty($component['extra']['skip_button_page']) ? $component['extra']['skip_button_page'] : '';
  $skip_button = !empty($component['extra']['skip_button_label']) ? t($component['extra']['skip_button_label']) : t('Skip');

  $element = array(
    '#type' => 'submit',
    '#value' => $skip_button,
    '#weight' => $component['weight'],
    '#attributes' => array(
      'class' => array(
        'form-item',
        'webform-component',
        'webform-skip-button',
        $skip_button_alignment,
      ),
      'id' => array(
        $skip_button_trigger,
      ),
    ),
  );

  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_skip_button($component, $value = NULL, $format = 'html', $submission = array()) {
  $element = array(
    '#theme' => 'webform_display_skip_button',
    '#title' => $component['name'],
    '#weight' => $component['weight'],
    '#format' => $format,
    '#translatable' => array('title', 'skip_button_label'),
  );

  return $element;
}

/**
 * Format the text output data for this component.
 */
function theme_webform_display_skip_button($variables) {
  $element = $variables['element'];

  return $element['#format'] == 'html' ? '<h2 class="webform-page">' . check_plain($element['#title']) . '</h2>' : '==' . $element['#title'] . "==\n";
}

/**
 * Callback to retrieve webform fields as select options.
 *
 * @return array
 */
function _webform_skip_button_get_page_options($component) {
  // Get defined skip_button component
  $skip_button = $component;

  // Get the node number for the current webform
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    // Get the nid
    $nid = arg(1);

    // Load the node to get all components
    $node = node_load($nid);

    // Set default option
    $valid_components = array();
    $options = array('0' => '- Select -');
    $diffpage = array();
    $samepage = array();
    $components = $node->webform['components'];
    foreach ($components as $key => $component) {
      if ($component['type'] == 'pagebreak' || $component['type'] == 'skip') {
        unset($components[$key]);
      }
      else {
        $valid_components[$key] = $component;
      }
    }
    foreach ($valid_components as $samepage_component) {
      if ($samepage_component['page_num'] === $skip_button['page_num']) {
        $samepage[$samepage_component['form_key']] = $samepage_component['name'] . ' (On same page) ';
      }
    }
    // Prevent skipping required fields, show next required field(s).
    $required_components = array();
    $non_required_components = array();
    foreach ($valid_components as $key => $component) {
      if ($component['required'] == 1) {
        $required_components[$key] = $component;
      }
      else {
        $non_required_components[$key] = $component;
      }
    }
    // Compare page numbers of required fields so you can't skip past them
    foreach ($required_components as $required_key => $required_component) {
      foreach ($non_required_components as $component_not_required_key => $component_not_required) {
        if ($required_component['page_num'] >= $component_not_required['page_num'] && $component_not_required['page_num'] >= $skip_button['page_num']) {
          if ($component_not_required['page_num'] !== $skip_button['page_num']) {
            $diffpage[$component_not_required['page_num']] = $component_not_required['name'] . ' (Page ' . $component_not_required['page_num'] . ')';
          }
          if ($required_component['page_num'] == $component_not_required['page_num'] + 1) {
            $required_component[$required_component['page_num']] = $required_component['name'] . ' (Required component: Page ' . $required_component['page_num'] . ')';
            drupal_set_message(t('There are required fields preventing you from skipping further pages. Moving the skip button past the required component, or removing the required field(s), will allow more options to show in this form.'), 'warning');
          }
          if (isset($required_component) && is_array($required_component)) {
            $diffpage = array_replace($diffpage, $required_component);
            ksort($diffpage);
          } else {
            ksort($diffpage);
          }
        }
      }
    }
    // if there are no required fields on different pages, show all fields.
    if (empty($diffpage)) {
      foreach ($valid_components as $key => $component) {
        if ($component['page_num'] > $skip_button['page_num'] && $component['page_num'] !== $skip_button['page_num']) {
          $diffpage[$component['page_num']] = $component['name'] . ' (Page ' . $component['page_num'] . ')';
        }
      }
    }
  }

  // Merge different arrays
  $options = array_replace($options, $samepage, $diffpage);

  return $options;
}
