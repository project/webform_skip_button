CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers



INTRODUCTION
------------

This module solves the issue of skipping pages of optional content on long single page or multipage webforms.
The module provides a skip button component that allows you to link to fields on the same page or different pages on
the same webform. If there is a required field on multipage webforms the button detects them automatically
and prevents you from skipping past them (forcing you to select this component or another optional one on a
previous page of the form).

 * For a full description of the module, visit the project page:
   https://drupal.org/project/webform_skip_button

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/webform_skip_button



REQUIREMENTS
------------

This module requires the following modules:

 * Webform (https://drupal.org/project/webform)



INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.



CONFIGURATION
-------------

 * Configure new component under Webform » Skip button:

   - Enter a button label, select a component the button should link to, and your alignment (left or right)



MAINTAINERS
-----------

Current maintainers:
 * Dan Fitzsimmons (dpfitzsi) - https://drupal.org/user/707988

This project has been sponsored by:
 * 76design